﻿window.onload = function () {

    // RaphaelTris
    // by Mike Huttman

    // Raphael documentation: http://raphaeljs.com/reference.html

    // Raphael(x pos, y pos, width, height)
    var paper = Raphael(document.getElementById('canvas'), 800, 800);

    // Circle(center x pos, center y pos, radius) 
    var c = paper.circle(50, 50, 40);

    c.attr("fill", "red");

    // in Javascript, use == for equals comparison (http://www.w3schools.com/js/js_comparisons.asp)
    // node.(event) is the format that raphael uses for event handling
    // In this case I am checking to see what color my circle is, and then swapping for the other color
    c.node.onclick = function () {
        if (c.attr("fill") == "blue") {
            c.attr("fill", "red");
        }
        else {
            c.attr("fill", "blue");
        }
    }

    var colors = ["Yellow", "Green", "Red", "#66FFFF", "Orange"];
    var circles = new Array();

    // Five circles in a row
    //            for (var i = 0; i < 6; i += 1) {
    //                var mult = i * 85;
    //                paper.circle(20 + mult, 250, 40).attr("fill", colors[i - 1]);

    //            }

    // Tetronimo (path)
    /*
                
    Path commands
    M = move the cursor to x y position
    l = draw from last cursor position x y pixels
    x positive = right
    y positive = down
    z = end path
    */

    // T shape

    var tetronimo = paper.path("M 100 150 l 0 -5 l -5 0 l 0 -5 l -5 0 l 0 5 l -5 0 l 0 5 z").attr("fill", "#33CCFF");
    var tetronimo2 = paper.path("M 100 170 l 0 15 l 5 0 l 0 -5 l 5 0 l 0 -5 l -5 0 l 0 -5 l -5 0 z").attr("fill", "#33CCFF");
    var tetromimo3 = paper.path("M 100 190 l 15 0 l 0 5 l -5 0 l 0 5 l -5 0 l 0 -5 l -5 0 l 0 -5 z").attr("fill", "#33CCFF");
    var tetronimo4 = paper.path("M 100 210 l 0 15 l -5 0 l 0 -5 l -5 0 l 0 -5 l 5 0 l 0 -5 l 5 0 z").attr("fill", "#33CCFF");

    //var text = paper.print(100, 100, "m",, 24).attr({ fill: "#fff" });

    // Jagged (facing left)
    var jagged = paper.path("M 150 150 l 0 5 l 10 0 l 0 -5 l 5 0 l 0 -5 l -10 0 l 0 5 z").attr("fill", "#99FF33");
    var jagged2 = paper.path("M 150 170 l 0 10 l 5 0 l 0 5 l 5 0 l 0 -10 l -5 0 l 0 -5 l -5 0 z").attr("fill", "#99FF33");

    // Jagged (facing right)
    var jaggedR = paper.path("M 200 150 l 0 5 l 5 0 l 0 5 l 10 0 l 0 -5 l -5 0 l 0 -5 l -10 0 z").attr("fill", "#FF6600");
    var jaggedR2 = paper.path("M 200 170 l 0 5 l -5 0 l 0 10 l 5 0 l 0 -5 l 5 0 l 0 -10 l -5 0 z").attr("fill", "#FF6600");

    // Square
    var square = paper.path("M 250 150 l 0 10 l 10 0 l 0 -10 l -10 0 z").attr("fill", "Yellow");

    // Line
    var lineBlock = paper.path("M 300 150 l 0 20 l 5 0 l 0 -20 l -5 0 z").attr("fill", "#FF0066");
    var lineBlock = paper.path("M 300 180 l 20 0 l 0 -5 l -20 0 l 0 5 z").attr("fill", "#FF0066");

    // L (facing left)
    var lLeft = paper.path("M 350 150 l 0 15 l -5 0 l 0 5 l 10 0 l 0 -20 l -5 0 z").attr("fill", "#FF99FF");
    var lLeft1 = paper.path("M 350 180 l 0 10 l 20 0 l 0 -5 l -15 0 l 0 -5 l -5 0 z").attr("fill", "#FF99FF");
    var lLeft2 = paper.path("M 350 210 l 0 20 l 5 0 l 0 -15 l 5 0 l 0 -5 l -10 0 z").attr("fill", "#FF99FF");
    var lLeft3 = paper.path("M 350 240 l 0 5 l 15 0 l 0 5 l 5 0 l 0 -10 l -20 0 z").attr("fill", "#FF99FF");

    // L (facing right)
    var lRight = paper.path("M 400 150 l 0 20 l 10 0 l 0 -5 l -5 0 l 0 -15 l -5 0 z").attr("fill", "#53CDAE");
    var lRight1 = paper.path("M 400 180 l 20 0 l 0 5 l -15 0 l 0 5 l -5 0 l 0 -10 z").attr("fill", "#53CDAE");
    var lRight2 = paper.path("M 400 210 l 0 5 l 5 0 l 0 15 l 5 0 l 0 -20 l -10 0 z").attr("fill", "#53CDAE");
    var lRight3 = paper.path("M 400 240 l 15 0 l 0 -5 l 5 0 l 0 10 l -20 0 l 0 -5 z").attr("fill", "#53CDAE");
}
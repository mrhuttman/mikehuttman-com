﻿// Before / After Gallery
/*
    Using jQuery, this gallery cycles through images and allows me to switch from between two different images 
    (before & after) using onclick events per gallery item.
*/
$(document).ready(function ()
{
    var imgPrefix = "<img src='Images/WebDev/";
    var images_BA = [];
    images_BA[0] = "Aeroflow_Menus_Before.png";
    images_BA[1] = "Aeroflow_Menus_After.png";
    images_BA[2] = "Kelly_Boob_Book_Before.png";
    images_BA[3] = "Kelly_Boob_Book_After.png";
    images_BA[4] = "ATCB Before.jpg";
    images_BA[5] = "ATCB_After.png";
    images_BA[6] = "Boulevard Bazaar Before.jpg";
    images_BA[7] = "Boulevard Bazaar After.jpg";
    images_BA[8] = "HH ProductSearch Before.jpg";
    images_BA[9] = "HH ProductSearch After.jpg";

    var captions_BA = [];
    captions_BA[0] = "<b>Aeroflow Healthcare - BEFORE</b><br><i><a href='//www.aeroflowinc.com' target='_blank' >http://www.aeroflowinc.com</a></i><br><br><span>The old version of this site used menus from a WordPress template that didn't work well on mobile. The mobile menu would spill out over the page content, leading to a poor user experience.</span>";
    captions_BA[1] = "<b>Aeroflow Healthcare - AFTER</b><br><i><a href='//www.aeroflowinc.com' target='_blank' >http://www.aeroflowinc.com</a></i><br><br><span>The new version of this site uses a custom menu that is fully responsive while allowing for custom icons to appear next to each menu item or sub-menu link.</span>";
    captions_BA[2] = "<b>Breast Pump Reviews - BEFORE</b><br><i><a href='//www.kellyboobbook.com' target='_blank' >http://www.kellyboobbook.com</a></i><br><br><span>Breast Pump Reviews was a site that was supposed to be a hub for moms to post reviews of their breastfeeding equipment. The site's design was outdated and uninspired, leading to low traffic.</span>";
    captions_BA[3] = "<b>Breast Pump Reviews - AFTER</b><br><i><a href='//www.kellyboobbook.com' target='_blank' >http://www.kellyboobbook.com</a></i><br><br><span>I worked with a graphic designer to re-imagine Breast Pump Reviews as Kelly Boob Book. The new color scheme, modern fonts, and grid-based responsive layout made the site much more attractive. With this re-branding Kelly Boob Book is giving a stagnant site new life.</span>";
    captions_BA[4] = "<b>Art Therapy Credentials Board - BEFORE</b><br><i><a href='//www.atcb.org' target='_blank' >http://www.atcb.org</a></i><br><br><span>The old version of this site uses an outdated 'tabular' design. The entire page is coded into one large table, and code behind elements such as the side navigation links is replicated across pages due to poor structural planning. Finally, this version of the site had a 'DLL' problem due to version issues with the MVC DLL files.</span>";
    captions_BA[5] = "<b>Art Therapy Credentials Board - AFTER</b><br><i><a href='//www.atcb.org' target='_blank' >http://www.atcb.org</a> (Re-design concept)</i><br><br><span>The new version of this site uses updated Razor/MVC architecture to help prevent DLL problems. The design is updated with new CSS3 and HTML5 elements. Navigation is updated and streamlined. Finally, the tabular design is replaced with a more fluid CSS-centric design that separates the visual design from the Razor/HTML markup. </span>";
    captions_BA[6] = "<b>Boulevard Bazaar - BEFORE</b><br><i><a href='//www.boulevardbazaar.com/' target='_blank' >http://www.boulevardbazaar.com/</a></i><br><br><span>This is the website for the Boulevard Home Furnishings Bazaar in Charlotte, NC. Boulevard Bazaar sells high-end furniture and fabrics at discount prices, as well as offering re-upholstery services. <br><br>The original site used a flash-based image gallery of thumbnail images, giving the page a cluttered design.</span>";
    captions_BA[7] = "<b>Boulevard Bazaar - AFTER</b><br><i><a href='//www.boulevardbazaar.com/' target='_blank' >http://www.boulevardbazaar.com/</a></i><br><br><span>I completely re-designed this website to have a more standard, open design. By separating the site navigation and content areas into separate columns, the page gained a lot of space to include items like the larger-sized Javascript galleries on some of the site pages. Store hours and contact information are now dislayed in plain text instead of in an image on both the top and bottom headers of each page, which greatly improves this site's SEO efforts.</span>";
    captions_BA[8] = "<b>Highland House Product Search - BEFORE</b><br><i><a href='//www.highlandhousefurniture.com/Consumer/ProductSearch.aspx' target='_blank'>http://www.highlandhousefurniture.com/Consumer/ProductSearch.aspx</a></i><br><br><span >This is the product search page for Highland House Furniture's official site. The product categories were pulled from a database and populated in a list.</span>";
    captions_BA[9] = "<b>Highland House Product Search - AFTER</b><br><i><a href='//www.highlandhousefurniture.com/Consumer/ProductSearch.aspx' target='_blank'>http://www.highlandhousefurniture.com/Consumer/ProductSearch.aspx</a></i><br><br><span >I cleaned up the underlying data for the cateories, and rewrote this part of the page to display the categories with representative thumbnails, improving the overall look of this page and keeping it uniform with the other product listings.</span>";

    // Show first image 
    var curPos_BA = 0;
    $("#curImage_BA").html(imgPrefix + images_BA[0].toString() + "'>");
    $("#caption_BA").html(captions_BA[curPos_BA]);
    $("#baButton").html("Show After");

    // Load last image
    $("#prevButton_BA").click(function ()
    {
        switch (curPos_BA)
        {
            case 0:
            case 1:
                curPos_BA = images_BA.length - 2;
                break;
            case 2:
            case 3:
                curPos_BA = 0;
                break;
            case 4:
            case 5:
                curPos_BA = 2;
                break;
            case 6:
            case 7:
                curPos_BA = 4;
                break;  
            case 8:
            case 9:
                curPos_BA = 6;
                break;                           
            default:
                curPos_BA = 0;
                break;
        }
        $("#curImage_BA").html(imgPrefix + images_BA[curPos_BA].toString() + "'>");
        $("#baButton").html("Show After"); // Reset to Before to prevent After -> After double-click issue
        $("#caption_BA").html(captions_BA[curPos_BA]);
    }); // end prevButton.click

    // Load next image
    $("#nextButton_BA").click(function ()
    {
        switch (curPos_BA)
        {
            case 0:
            case 1:
                curPos_BA = 2;
                break;
            case 2:
            case 3:
                curPos_BA = 4;
                break;
            case 4:
            case 5:
                curPos_BA = 6;
                break;
            case 6:
            case 7:
                curPos_BA = 8;
                break;             
            case 8:
            case 9:
                curPos_BA = 10;
                break;            
            default:
                curPos_BA = 0;
                break;
        }
        $("#curImage_BA").html(imgPrefix + images_BA[curPos_BA].toString() + "'>");
        $("#baButton").html("Show After"); // Reset to Before to prevent After -> After double-click issue
        $("#caption_BA").html(captions_BA[curPos_BA]);
    }); // end nextButton.click

    // Before / After toggle
    $("#baButton").click(function ()
    {
        // Toggle the text on the before/after button
        if ($("#baButton").html() == "Show Before")
        {
            $("#baButton").html("Show After");
            $("#curImage_BA").html(imgPrefix + images_BA[curPos_BA].toString() + "'>");
            $("#caption_BA").html(captions_BA[curPos_BA]);
        }
        else
        {
            $("#baButton").html("Show Before");
            $("#curImage_BA").html(imgPrefix + images_BA[curPos_BA + 1].toString() + "'>");
            $("#caption_BA").html(captions_BA[curPos_BA + 1]);
        } // end if
    }); // end baButton.click
});
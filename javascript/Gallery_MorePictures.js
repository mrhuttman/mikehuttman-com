﻿// More Pictures
$(document).ready(function ()
{       
    var imgPrefix = "<img src='Images/WebDev/";
    var captionSelector = "#caption_MorePictures";
    var imgSelector = "#curImage_MorePictures";
    var previousButtonSelector = "#prevButton_MorePictures";
    var nextButtonSelector = "#nextButton_MorePictures";

    var galleryMorePictures = [];
    galleryMorePictures.push(new CaptionImage({
        "image": "Elyse Dashew Experience.png",
        "caption": "<b>Elyse Dashew | Experience & Endorsements</b><br><i><a href='//elysedashew.com/experienceendorsements/' target='_blank'>http://elysedashew.com/experienceendorsements/</a></i><br><br><span>This page uses an accordion that was customized to match the visual aesthetic of the rest of the page.</span>"
    }));
    galleryMorePictures.push(new CaptionImage({
        "image": "Breast_Pumps_Video_Resources.png",
        "caption": "<b>Aeroflow Breastpumps | Video Resources</b><br><i><a href='https://aeroflowbreastpumps.com/video-resources' target='_blank'>https://aeroflowbreastpumps.com/video-resources</a></i><br><br><span>My code is present on many pages across this site. This page shows how a CSS Grid layout can add some variety to a simple page by allowing video embeds to occupy varying amounts of screen space.</span>"
    }));
    galleryMorePictures.push(new CaptionImage({
        "image": "Breast_Pumps_Video_Resources_Mobile.png",
        "caption": "<b>Aeroflow Breastpumps | Video Resources</b><br><i><a href='https://aeroflowbreastpumps.com/video-resources' target='_blank'>https://aeroflowbreastpumps.com/video-resources</a></i><br><br><span>Using a CSS grid system I am able to create web pages that look great on smart phones, desktop screens, and everything in-between with minimal errort.</span>"
    }));
    galleryMorePictures.push(new CaptionImage({
        "image": "Frontend_CEOpportunities.png",
        "caption": "<b>CCE Global | Continuing Education Opportunities</b><br><i><a href='//www.cce-global.org/BCC/ContinuingEducationOpportunities' target='_blank'>http://www.cce-global.org/BCC/ContinuingEducationOpportunities</a></i><br><br><span>This design replaced a previous box-based design, which was hard for prospective students to naviagate. This implementation uses a sortable table with additional info available under each row after clicking \"Show Contact Info.\" In order to accomodate the long column names, I used CSS3 techniques to create the slanted column headings. The end result is a page that is visually appealing while being informative for the intended audience.</span>"
    }));   
    galleryMorePictures.push(new CaptionImage({
        "image": "AsSeenIn.jpg",
        "caption": "<b>Century Furniture | As Seen In</b><br><i><a href='//www.centuryfurniture.com/PressReleases/default.aspx?Show=ProdInPrint' target='_blank' >http://www.centuryfurniture.com/PressReleases/default.aspx?Show=ProdInPrint</a></i><br><br><span>Century Furniture's official page. Uses an ASP.NET repeater control along with an SQL query to dynamically produce this table.</span>"
    }));

    // Show first image 
    var curPos_BackEnd = 0;
    $(imgSelector).html(imgPrefix + galleryMorePictures[curPos_BackEnd].image + "'>");
    $(captionSelector).html(galleryMorePictures[curPos_BackEnd].caption);

    // Load last image
    $(previousButtonSelector).click(function () {
        if (curPos_BackEnd > 0) { curPos_BackEnd--; }
        else { curPos_BackEnd = galleryMorePictures.length - 1; } // loop back to the end 
        $(imgSelector).html(imgPrefix + galleryMorePictures[curPos_BackEnd].image + "'>");
        $(captionSelector).html(galleryMorePictures[curPos_BackEnd].caption);
    }); // end prevButton.click

    // Load next image
    $(nextButtonSelector).click(function () {
        if (curPos_BackEnd < (galleryMorePictures.length - 1)) { curPos_BackEnd++; }
        else { curPos_BackEnd = 0; } // loop back to the beginning
        $(imgSelector).html(imgPrefix + galleryMorePictures[curPos_BackEnd].image + "'>");
        $(captionSelector).html(galleryMorePictures[curPos_BackEnd].caption);
    }); // end nextButton.click
});
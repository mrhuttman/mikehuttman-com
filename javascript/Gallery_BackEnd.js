﻿// Back End Gallery
$(document).ready(function ()
{
    var imgPrefix = "<img src='Images/WebDev/";
    var galleryBackEnd = [];
    galleryBackEnd.push(new CaptionImage({
        "image": "Real_Time_Inventory.png",
        "caption": "<b>Real-Time Inventory Dashboard (internal project)</b><br><br><span>I built a \"real-time\" inventory dashboard that reported item stock & location information. The dashboard used PHP, called via AJAX, to query inventory data from an internal SQL server, and then used DataTables to display/paginate through the results.</span>"
    }));
    galleryBackEnd.push(new CaptionImage({
        "image": "Backend_hsepAdd.jpg",
        "caption": "<b>Human Services Educational Directory | Join The Directory </b><br><i><a href='//humanservices.cce-global.org/home/getlisted' target='_blank'>http://humanservices.cce-global.org/home/getlisted</a></i><br><br><span >This page uses input validation and reCaptcha to check for human input before sending an email request for a new school to be added to the directory.</span>"
    }));
    galleryBackEnd.push(new CaptionImage({
        "image": "FabricDetail.jpg",
        "caption": "<b>Century Furniture | Fabric Detail</b><br><i><a href='//www.centuryfurniture.com/Fabrics/FabricDetail.aspx?sku=20482L23' target='_blank'>http://www.centuryfurniture.com/Fabrics/FabricDetail.aspx?sku=20482L23</a></i><br><br><span >Century Furniture's official page. An SQL query and an ASP DataList control populates the Other Available Color Ways panel.</span>"
    }));

    // Show first image 
    var curPos_BackEnd = 0;
    $("#curImage_Backend").html(imgPrefix + galleryBackEnd[curPos_BackEnd].image + "'>");
    $("#caption_Backend").html(galleryBackEnd[curPos_BackEnd].caption);

    // Load last image
    $("#prevButton_Backend").click(function ()
    {
        if (curPos_BackEnd > 0) { curPos_BackEnd--; }
        else { curPos_BackEnd = galleryBackEnd.length - 1; } // loop back to the end 
        $("#curImage_Backend").html(imgPrefix + galleryBackEnd[curPos_BackEnd].image + "'>");
        $("#caption_Backend").html(galleryBackEnd[curPos_BackEnd].caption);
    }); // end prevButton.click

    // Load next image
    $("#nextButton_Backend").click(function ()
    {
        if (curPos_BackEnd < (galleryBackEnd.length - 1)) { curPos_BackEnd++; }
        else { curPos_BackEnd = 0; } // loop back to the beginning
        $("#curImage_Backend").html(imgPrefix + galleryBackEnd[curPos_BackEnd].image + "'>");
        $("#caption_Backend").html(galleryBackEnd[curPos_BackEnd].caption);
    }); // end nextButton.click
});
﻿// Defense of Marcabia Gallery
$(document).ready(function ()
{
    var imgPrefix = "<img src='Images/GameDemos/";
    var captionSelector = "#caption_Marcabia";
    var imgSelector = "#curImage_Marcabia";
    var previousButtonSelector = "#prevButton_Marcabia";
    var nextButtonSelector = "#nextButton_Marcabia";

    var galleryMarcabia = [];
    galleryMarcabia.push(new CaptionImage({
        "image": "Marcabiashot6.jpg",
        "caption": "<b>Defence of Marcabia | Title Screen</b>"
    }));
    galleryMarcabia.push(new CaptionImage({
        "image": "Marcabiashot4.jpg",
        "caption": "<b>Defence of Marcabia | 2nd level boss</b>"
    }));
    galleryMarcabia.push(new CaptionImage({
        "image": "Marcabiashot3.jpg",
        "caption": "<b>Defence of Marcabia | 1st level boss</b>"
    }));
    galleryMarcabia.push(new CaptionImage({
        "image": "Marcabiashot5.jpg",
        "caption": "<b>Defence of Marcabia | 1st level enemies</b>"
    }));

    // Show first image 
    var currentPositionMarcabia = 0;
    $("#curImage_Marcabia").html(imgPrefix + galleryMarcabia[currentPositionMarcabia].image + "'>");
    $("#caption_Marcabia").html(galleryMarcabia[currentPositionMarcabia].caption);

    // Load last image
    $("#prevButton_Marcabia").click(function ()
    {
        if (currentPositionMarcabia > 0) { currentPositionMarcabia--; }
        else { currentPositionMarcabia = galleryMarcabia.length - 1; } // loop back to the end 
        $("#curImage_Marcabia").html(imgPrefix + galleryMarcabia[currentPositionMarcabia].image + "'>");
        $("#caption_Marcabia").html(galleryMarcabia[currentPositionMarcabia].caption);
    }); // end prevButton.click

    // Load next image
    $("#nextButton_Marcabia").click(function ()
    {
        if (currentPositionMarcabia < (galleryMarcabia.length - 1)) { currentPositionMarcabia++; }
        else { currentPositionMarcabia = 0; } // loop back to the beginning
        $("#curImage_Marcabia").html(imgPrefix + galleryMarcabia[currentPositionMarcabia].image + "'>");
        $("#caption_Marcabia").html(galleryMarcabia[currentPositionMarcabia].caption);
    }); // end nextButton.click

});
﻿// Photoshop
$(document).ready(function ()
{
    var imgPrefix = "<img src='Images/WebDev/";
    var captionSelector = "#caption_Photoshop";
    var imgSelector = "#curImage_Photoshop";
    var previousButtonSelector = "#prevButton_Photoshop";
    var nextButtonSelector = "#nextButton_Photoshop";

    var galleryPhotoshop = [];
    galleryPhotoshop.push(new CaptionImage({
        "image": "Photoshop_NBCC_Mockup.png",
        "caption": "<span>This is a conceptual mock-up I did for one of NBCC's examinations pages.</span>"
    }));
    galleryPhotoshop.push(new CaptionImage({
        "image": "Elektro_Shock_Cover_575w.png",
        "caption": "<span>This image is used as a cover image for my <a href='DJ.aspx#Mixes' target='_blank'>Elektro Shock</a> mix. Several small tweaks came together to transform the source image into an inviting cover: I selected the colors for the bottom bar to complement the top image while maintaining a playful look. A custom font is used for the text, with the \"S\" in Shock replaced for effect. A Stroke effect is added to the letters to help them stand out against the pink background. I also added an \"electrifying\" touch with layers of lightning near the defilrilator.</span>"
    }));
    galleryPhotoshop.push(new CaptionImage({
        "image": "Photoshop_Resume.png",
        "caption": "<span>I created a mock-up of a \"stylized\" one-page version of my resume. The full size version of this image is designed to be print-friendly. Icons from the Font Awesome set are used for graphical flourish.</span>"
    }));
    
    // Show first image 
    var currentPositionPS = 0;
    $(imgSelector).html(imgPrefix + galleryPhotoshop[currentPositionPS].image + "'>");
    $(captionSelector).html(galleryPhotoshop[currentPositionPS].caption);

    // Load last image
    $("#prevButton_Photoshop").click(function ()
    {
        if (currentPositionPS > 0) { currentPositionPS--; }
        else { currentPositionPS = galleryPhotoshop.length - 1; } // loop back to the end 
        $(imgSelector).html(imgPrefix + galleryPhotoshop[currentPositionPS].image + "'>");
        $(captionSelector).html(galleryPhotoshop[currentPositionPS].caption);
    }); // end prevButton.click

    // Load next image
    $("#nextButton_Photoshop").click(function ()
    {
        if (currentPositionPS < (galleryPhotoshop.length - 1)) { currentPositionPS++; }
        else { currentPositionPS = 0; } // loop back to the beginning
        $(imgSelector).html(imgPrefix + galleryPhotoshop[currentPositionPS].image + "'>");
        $(captionSelector).html(galleryPhotoshop[currentPositionPS].caption);
    }); // end nextButton.click
});
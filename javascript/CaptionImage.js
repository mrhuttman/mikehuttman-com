﻿function CaptionImage(args)
{
    var ci = { "caption": "", "image": "" };
    for (var i = 0; i < arguments.length; i++)
    {
        if (arguments[i].caption) { ci.caption = arguments[i].caption; }
        if (arguments[i].image) { ci.image = arguments[i].image; }
    }    
    return ci;
}
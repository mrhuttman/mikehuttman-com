﻿window.onload = function () {

    // Declare variables
    var canvas = new Raphael("canvas", 855, 855); //our Raphael canvas handler

    var debugString = ""; //used to output debug text
    var statusString = ""; //used to output status text

    var startx = 27; //starting coordinates for the grid
    var starty = 25;
    var width = 100; //the size of each rect
    var height = 100;
    var cornerRadius = 5; //the number of pixels that we will round each tile's corners

    var colors = new Array(8);
    colors[0] = "#FF99FF"; // Pink
    colors[1] = "#66FF66"; // Green
    colors[2] = "#FF9933"; // Orange
    colors[3] = "#33CCFF"; // Blue
    colors[4] = "#FFFF66"; // Yellow
    colors[5] = "#FF4719"; // Red
    colors[6] = "#33CCCC"; // Blue-Green
    colors[7] = "#9FBFBF"; // Silver

    // Setup our game board
    // First we create our base array (columns), then create arrays per each element (rows)
    var grid = new Array(8);

    for (var i = 0; i < grid.length; i++) {

        grid[i] = new Array(8);

    }

    // drawRect
    /*
    
    returns a Raphael rectangle object with the inputted options
    color = the value that the color attr will be set to for this rect
    x, y = x & y position of the top-left corner of the rect
    w, h = width and height of the rect
    corner = radius for the rounded corners
    
    */
    function drawRect(color, x, y, w, h, corner) {

        var temp = canvas.rect(x, y, w, h, corner);
        temp.attr("fill", color);
        temp.node.setAttribute("selected", "false"); // Will be used to check for selected tiles in main loop

        // Add the mouseover/hover events
        temp.node.onmouseover = function () {

            temp.animate({ scale: 1.5, stroke: "#FFF", 'stroke-width': 3 }, 250, 'bounce').toFront();

        }; // end temp.node.onmouseover

        temp.node.onmouseout = function () {

            if (temp.node.getAttribute("selected") == "true") {

                temp.animate({ scale: 1, stroke: "#FFF", 'stroke-width': 4 }, 250, 'elastic').toFront();

            }
            else {

                temp.animate({ scale: 1, stroke: "#000", 'stroke-width': 2 }, 250, 'elastic').toBack();

            }

            // Update the status debugging
            statusString += "<br>(x: " + x.toString() + ", y: " + y.toString() + ") temp.node.getAttribute('selected') after .node.onmouseout = " + temp.node.getAttribute("selected");
            $("#status").html(statusString);
            statusString = "";

        }; // end temp.node.onmouseout

        // Toggle between selected / not-selected
        temp.node.onclick = function () {

            // if not selected yet
            if (temp.node.getAttribute("selected") == "false") {

                temp.animate({ scale: 1, stroke: "#FFF", 'stroke-width': 4 }, 250, 'elastic').toFront();
                temp.node.setAttribute("selected", "true");

            }
            // if already selected de-select
            else {

                temp.animate({ scale: 1, stroke: "#000", 'stroke-width': 2 }, 250, 'elastic').toBack();
                temp.node.setAttribute("selected", "false");
                
            }
            
            // Update the status debugging
            statusString += "<br>(x: " + x.toString() + ", y: " + y.toString() + ") temp.node.getAttribute('selected') after .node.onclick = " + temp.node.getAttribute("selected");
            $("#status").html(statusString);
            statusString = "";

        }; // end temp.node.onclick        

        return temp;

        /*

        NOTES:

        temp.node.[function] - the node raphael property lets me directly access the DOM element that I just created
                
        I will need to add all of my onclick and other direct access events for my tiles in this function.
        I will get errors if I try to reference the DOM elements from another part of this program, probably because it is only
        in this function that I actually create the DOM object. Once this function finishes, then the temp object reference is passed 
        back into my initialization loop, where at that point I cannot add any more DOM elements. 

        .animate - Raphael's animation function. Note that the properties that are animated are defined in the first argument,
        between a set of {}. The second argument is the time in milliseconds that the animation will take.
               
        .toFront() & .toBack() - These functions move the object to the top or bottom of the drawing stack. In other words, if I use 
        toFront on an object, it should be drawn on top of everything else, and vice-versa. This eliminates a graphical issue where the 
        tiles overrlap while expanded.

        .node.setAttribute / .getAttribute(key, value) - These are the DOM functions to set and get attributes of specific DOM elements.
        I am using these instead of .attr(attr, value) because I can't perform jQuery operations on the DOM elements at this point in the script.

        */

    } // end drawRect

    // Initialize the grid of rectangles
    for (var i = 0; i < grid.length; i++) {

        for (var j = 0; j < grid[i].length; j++) {

            grid[i][j] = drawRect(colors[Math.floor(Math.random() * 8)], startx + (width * j), starty + (height * i), width, height, cornerRadius);

            // used for debugging
            debugString += "<br>grid[" + i + "][" + j + "] x: " + startx + (width * j) + " y: " + starty + (height * i);

        } // end inner for loop

    } // end outer for loop

    debugString += "<br>grid.length: " + grid.length;
    debugString += "<br>grid[1].length: " + grid[1].length;
    $("#debug").html(debugString);

    // Main program loop
    (function () {

    } // end main loop
    )();

}; // end window.onload


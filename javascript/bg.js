﻿$(document).ready(function () {

    // The list of bg images to work with - http://subtlepatterns.com/
    var backgroundURLs = [];
    backgroundURLs[0] = "Images/bg/congruent_outline.png";    
    backgroundURLs[1] = "Images/bg/triangular.png";
    backgroundURLs[2] = "Images/bg/wild_oliva.png";

    var curImg = "";
    var lastImg = "";

    // Get a random image URL from the list
    function loadRandomImage(URLs) {
        
        return URLs[Math.floor(Math.random() * URLs.length)];

    }

    function loopBG() {

        window.setInterval(function () {

            curImg = loadRandomImage(backgroundURLs);

            // Prevent repeats
            while (lastImg == curImg) {
                curImg = loadRandomImage(backgroundURLs);
            }

            $("#outer-container").css({

                WebkitTransition : 'background-image 2s ease-out',
                MozTransition    : 'background-image 2s ease-out',
                MsTransition     : 'background-image 2s ease-out',
                OTransition      : 'background-image 2s ease-out',
                transition       : 'background-image 2s ease-out'
            });

            // Wait a second, then set bg image and fade back in. Used to prevent a 'jarring' animation where the new bg appears for a split second.
            window.setTimeout(function () {

                $("#outer-container").css({
                'background-image': 'url(' + curImg + ')',
                WebkitTransition : 'background-image 2s ease-in',
                MozTransition    : 'background-image 2s ease-in',
                MsTransition     : 'background-image 2s ease-in',
                OTransition      : 'background-image 2s ease-in',
                transition       : 'background-image 2s ease-in'
                });

            }, 2000);
            
            lastImg = curImg;

        }, 30000); // Repeat indefinitely every x seconds, where x is the 2nd argument / 1000
    }
    
    // Setup initial bg variables
    curImg = loadRandomImage(backgroundURLs);
    if ($(window).width() > 625)
    {
        $("#outer-container").css('background-image', 'url(' + curImg + ')');
    }
    else
    {
        // On mobile, set the bg color to match the main container
        $("#outer-container").css('background-color', '#555');
    }

    lastImg = curImg;

    // Change bg in a loop (non-mobile)
    if ($(window).width() > 625) { loopBG(); }
    
});
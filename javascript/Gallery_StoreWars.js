﻿// Store Wars Gallery
$(document).ready(function ()
{
    var imgPrefix = "<img src='Images/GameDemos/";
    var captionSelector = "#caption_StoreWars";
    var imgSelector = "#curImage_StoreWars";
    var previousButtonSelector = "#prevButton_StoreWars";
    var nextButtonSelector = "#nextButton_StoreWars";

    var galleryStoreWars = [];
    galleryStoreWars.push(new CaptionImage({
        "image": "Mar2DeliLighting.jpg",
        "caption": "<b>Store Wars | Deli </b>"
    }));
    galleryStoreWars.push(new CaptionImage({
        "image": "Mar2DeliLighting2.jpg",
        "caption": "<b>Store Wars | Deli </b>"
    }));
    galleryStoreWars.push(new CaptionImage({
        "image": "Mar2MaterialEditor.jpg",
        "caption": "<b>Store Wars | Material Editor </b>"
    }));
    galleryStoreWars.push(new CaptionImage({
        "image": "Feb15Kismet.jpg",
        "caption": "<b>Store Wars | Kismet </b>"
    }));

    // Show first image 
    var curPos_StoreWars = 0;
    $("#curImage_StoreWars").html(imgPrefix + galleryStoreWars[curPos_StoreWars].image + "'>");
    $("#caption_StoreWars").html(galleryStoreWars[curPos_StoreWars].caption);

    // Load last image
    $("#prevButton_StoreWars").click(function ()
    {
        if (curPos_StoreWars > 0) { curPos_StoreWars--; }
        else { curPos_StoreWars = galleryStoreWars.length - 1; } // loop back to the end 
        $("#curImage_StoreWars").html(imgPrefix + galleryStoreWars[curPos_StoreWars].image + "'>");
        $("#caption_StoreWars").html(galleryStoreWars[curPos_StoreWars].caption);
    }); // end prevButton.click

    // Load next image
    $("#nextButton_StoreWars").click(function ()
    {
        if (curPos_StoreWars < (galleryStoreWars.length - 1)) { curPos_StoreWars++; }
        else { curPos_StoreWars = 0; } // loop back to the beginning
        $("#curImage_StoreWars").html(imgPrefix + galleryStoreWars[curPos_StoreWars].image + "'>");
        $("#caption_StoreWars").html(galleryStoreWars[curPos_StoreWars].caption);
    }); // end nextButton.click
});
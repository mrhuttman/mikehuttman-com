﻿$(document).ready(function () {

    // Setup the closeexpand-btn click event
    $("#closeexpand-btn").click(function () {

        if ($(this).hasClass("closeexpand-btn-close")) {

            // Close the top menu
            $("#sideMenu").slideUp();

            // Switch the button image 
            $(this).removeClass("closeexpand-btn-close");
            $(this).addClass("closeexpand-btn-expand");

        }
        else {

            // Expand the top menu
            $("#sideMenu").slideDown();

            // Switch the button image 
            $(this).addClass("closeexpand-btn-close");
            $(this).removeClass("closeexpand-btn-expand");

        }

    }); // end closeexpand-btn click function

    // Initialize button to expand, init menu closed if we are below 625px wide.            
    if ($(window).width() <= 625) {
        $("#sideMenu").slideUp();
        $("#closeexpand-btn").addClass("closeexpand-btn-expand");
    }
    else {
        $("#closeexpand-btn").addClass("closeexpand-btn-close");
    }

});
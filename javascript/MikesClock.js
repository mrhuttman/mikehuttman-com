// from http://raphaeljs.com/polar-clock.html
window.onload = function () {

    var r = new Raphael("holder", 400, 400); //our Raphael canvas handler
    var debugString = ""; //used to output debug text
    var h, m, s; // hours, minutes, seconds
    var sCircles = [60]; //seconds
    var hCircles = [24]; //hours
    var mCircles = [60]; //minutes    
    var radius = 10;


    // drawCircle
    /*

    Draws a circle at the inputted location on the canvas, with specified radius and color
    r = Raphael object, needed to avoid "object is not defined" issue
    color = Hex value (#FFFFFF)
    x, y = ints
    radius = double/float

    */
    function drawCircle(color, x, y, radius) {

        var temp = r.circle(x, y, radius);
        temp.attr("fill", color);

        return temp;

    } // end drawCircle


    // initCircles
    /*

    Initializes arrays of circles and sets them up in Raphael. 
    I will reveal or hide these circles based on the current time, hence creating a circle clock

    */

    // The base colors for each array of circles
    var secColor = "#19D175";
    var hourColor = "#FF7519";
    var minColor = "#47D1FF";

    // Starting positions for the first circle in each array
    var secStartx = 260;
    var secStarty = 380;
    var hourStartx = 45;
    var hourStarty = 380;
    var minStartx = 125;
    var minStarty = 380;

    var count = 0; // Our loops counter
    var curX = 0; // Used to keep track of the current element's x position

    debugString += "<br>***Seconds***";

    // Populate the seconds array
    for (count = 0; count <= 59; count += 1) {

        // Determine how far horizontally to place the current cirle
        // Every evenly divided count is in the leftmost position, every 5th circle after that shifts 20 px right
        switch (count % 5) {

            case 0:
                curX = 0;
                break;
            case 1:
                curX = 20;
                break;
            case 2:
                curX = 40;
                break;
            case 3:
                curX = 60;
                break;
            case 4:
                curX = 80;
                break;
            default:
                break;

        } // end switch

        // Adds a circle object to our array at the current position
        // The y coordinate is calculated as follows:
        /*
        Math.Floor(count / 5) determines which "row" we're on.
        For example, if count is 14, then Math.Floor(14 / 5) = 2, so we are two rows above the base row.
        That expression will resolve 0 for the first five counts (0-4) creating our base row.
        For every "row" above that, we move our circle up 15px.
        
        */
        sCircles[count] = drawCircle(secColor, secStartx + curX, secStarty - (20 * Math.floor(count / 5)), radius);

        // Used for debugging
        debugString += "<br>Circle #" + count + ": x:" + (secStartx + curX).toString() + " y:" + (secStarty - (20 * Math.floor(count / 5))).toString();
        debugString += "<br>secStartx = " + secStartx.toString() + ", curX = " + curX.toString() + ", count % 5 = " + (count % 5).toString();

    } // end seconds loop

    debugString += "<br>***Minutes***";

    // Populate the minutes array
    for (count = 0; count <= 59; count += 1) {

        // Determine how far horizontally to place the current cirle
        // Every evenly divided count is in the leftmost position, every 5th circle after that shifts 15 px right
        switch (count % 5) {

            case 0:
                curX = 0;
                break;
            case 1:
                curX = 25;
                break;
            case 2:
                curX = 50;
                break;
            case 3:
                curX = 75;
                break;
            case 4:
                curX = 100;
                break;
            default:
                break;

        } // end switch

        // Adds a circle object to our array at the current position
        // The y coordinate is calculated as follows:
        /*
        Math.Floor(count / 5) determines which "row" we're on.
        For example, if count is 14, then Math.Floor(14 / 5) = 2, so we are two rows above the base row.
        That expression will resolve 0 for the first five counts (0-4) creating our base row.
        For every "row" above that, we move our circle up 15px.
        
        */
        mCircles[count] = drawCircle(minColor, minStartx + curX, minStarty - (25 * Math.floor(count / 5)), (radius * 1.25));

        // Used for debugging
        debugString += "<br>Circle #" + count + ": x:" + (minStartx + curX).toString() + " y:" + (minStarty - (20 * Math.floor(count / 5))).toString();
        debugString += "<br>minStartx = " + minStartx.toString() + ", curX = " + curX.toString() + ", count % 5 = " + (count % 5).toString();

    } // end minutes loop

    debugString += "<br>***Hours***";

    // Populate the hours array
    for (count = 0; count <= 23; count += 1) {

        // Determine how far horizontally to place the current cirle
        // Every evenly divided count is in the leftmost position, every 5th circle after that shifts 15 px right
        switch (count % 2) {

            case 0:
                curX = 0;
                break;
            case 1:
                curX = 30;
                break;
            default:
                break;

        } // end switch

        // Adds a circle object to our array at the current position
        // The y coordinate is calculated as follows:
        /*
        Math.Floor(count / 5) determines which "row" we're on.
        For example, if count is 14, then Math.Floor(14 / 5) = 2, so we are two rows above the base row.
        That expression will resolve 0 for the first five counts (0-4) creating our base row.
        For every "row" above that, we move our circle up 15px.
        
        */
        hCircles[count] = drawCircle(hourColor, hourStartx + curX, hourStarty - (30 * Math.floor(count / 2)), (radius * 1.5));

        // Used for debugging
        debugString += "<br>Circle #" + count + ": x:" + (hourStartx + curX).toString() + " y:" + (hourStarty - (20 * Math.floor(count / 2))).toString();
        debugString += "<br>hourStartx = " + hourStartx.toString() + ", curX = " + curX.toString() + ", count % 2 = " + (count % 2).toString();

    } // end hours loop

    // Hide all circles
    // Seconds
    for (var i = 0; i < sCircles.length; i += 1) {
        sCircles[i].hide();
    }
    // Minutes
    for (var i = 0; i < mCircles.length; i += 1) {
        mCircles[i].hide();
    }
    // Hours
    for (var i = 0; i < hCircles.length; i += 1) {
        hCircles[i].hide();
    }
    // TODO: Figure out a better way to hide all of the circles instead of having a separate loop for each array
    // maybe setup a function that I could pass the array or name of the array into? I would also like this for the show() loops
    // that will need to be written

    // end initCircles


    // checkTime ()
    // returns a 0 in front of the time if it is < 10
    function checkTime(num) {

        if (num < 10) {

            num = "0" + num;

        }

        return num;
    } // end checkTime

    // updateCircles(s,m,h)
    // Shows the circles corresponding to the current time
    function updateCircles(s, m, h) {

        // Seconds
        for (var i = 0; i < s; i += 1) {

            sCircles[i].show();

        }

        // Minutes
        for (var i = 0; i < m; i += 1) {

            mCircles[i].show();

        }

        // Hours
        for (var i = 0; i < h; i += 1) {

            hCircles[i].show();
        }

        // Check for times of 0 so that we can reset the circles
        if (s == 0) {

            // Reset seconds circles
            for (var i = 0; i < sCircles.length; i += 1) {

                sCircles[i].hide();

            }

        }

        if (m == 0) {

            // Reset minutes circles
            for (var i = 0; i < mCircles.length; i += 1) {

                mCircles[i].hide();

            }

        }

        if (h == 0) {

            // Reset hours circles
            for (var i = 0; i < hCircles.length; i += 1) {

                hCircles[i].hide();

            }

        }

    } // end updateCircles


    // The main program loop
    (function () {

        var today = new Date();
        s = checkTime(today.getSeconds());
        m = checkTime(today.getMinutes());
        h = checkTime(today.getHours());
        $("#h").text(h.toString());
        $("#m").text(m.toString());
        $("#s").text(s.toString());

        updateCircles(s, m, h);

        // Start the infinite time loop
        /* setTimeout([do this], milliseconds): This performs the first argument after each number of specified milliseconds */
        // arguments.callee calls the current function. I'm not sure why I can't call loop() using this outside of the function
        setTimeout(arguments.callee, 1000);

    } //end main loop
    )();

};// end window.onload


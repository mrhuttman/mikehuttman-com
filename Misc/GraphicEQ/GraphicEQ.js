﻿var context = new webkitAudioContext();
var audioAnimation;
var audioBuffer;
var sourceNode;
var analyser;
var audio;

// get the context from the canvas to draw on
var canvas = document.getElementById('songcanvas');

var ctx = canvas.getContext("2d");

var gradient = ctx.createLinearGradient(0,0,0,canvas.height);
    gradient.addColorStop(1,'#00FF00');        // Green
    gradient.addColorStop(0.75,'#66FF66');     // #FF0000 Yellow
    gradient.addColorStop(0.25,'#66FFFF');     // #FF6600 Orange
    gradient.addColorStop(0,'#0066FF');        // #FF0000 Red

ctx.fillStyle = gradient;

function setupAudioNodes() {

  analyser = (analyser || context.createAnalyser());
  analyser.smoothingTimeConstant = 0.8;
  analyser.fftSize = 512;

  sourceNode = context.createMediaElementSource(audio);
  sourceNode.connect(analyser);
  sourceNode.connect(context.destination);  

  audio.play();
  drawSpectrum();
  console.log(audio.duration);
    
  audio.addEventListener("ended", function(){
  
      // When song is finished, revert back to default BG & show restart button
      ctx.clearRect(0, 0, WIDTH, HEIGHT);
      $('#songcanvas').hide();
      $('#bg').show();

      $('#logo_nav_pause').hide();
      $('#logo_nav_restart').show();

  });
  
}

function loadSong(url) {

  if (audio) audio.remove();
  if (sourceNode) sourceNode.disconnect();
  cancelAnimationFrame(audioAnimation);
  audio = new Audio();
  audio.src = url;  
  audio.addEventListener("canplay", function(e) {
    setupAudioNodes();
  }, false);

}

function drawSpectrum() {

  var WIDTH  = canvas.width,
      HEIGHT = canvas.height,
      array  = new Uint8Array(analyser.frequencyBinCount);
  
  analyser.getByteFrequencyData(array);
  
  ctx.clearRect(0, 0, WIDTH, HEIGHT);
  
  audioAnimation = requestAnimationFrame(drawSpectrum);
  
  for ( var i = 0; i < (array.length); i++ ){
    var value = array[i];
    
    // Calculate height from bottom
    // start x, start y, rect width, rect height
    // Rect is being drawn from the top!
    ctx.fillRect(                   i * 5,  // x
                -(HEIGHT - (value + 120)),  // y
                                        3,  // width
                                    HEIGHT  // height
                );
  }

}

$(document).ready(function(){

  // When one of the song links on the top bar are clicked
  $('.song').click(function(e){
    
    $('#logo_nav').hide();
    $('#logo_nav_pause').show();

    $('#bg').hide();
    $('#songcanvas').show();
  
    e.preventDefault();
    var path = $(this).attr('href');
    loadSong(path);
            
  });

  // When the center button is clicked
  $('#logo_nav').click(function(e){

    // If we already have audio, this button was clicked after audio was paused. Continue audio that was already loaded.
    if (audio)
    {
      
      $('#logo_nav').hide();
      $('#logo_nav_pause').show();
      
      $('#bg').hide();
      $('#songcanvas').show();

      audio.play();

    }
    else
    {

      // else, load the new song
      $('#logo_nav').hide();
      $('#logo_nav_pause').show();

      $('#bg').hide();
      $('#songcanvas').show();
  
      e.preventDefault();
      var path = "http://upload.wikimedia.org/wikipedia/en/4/45/ACDC_-_Back_In_Black-sample.ogg";
      loadSong(path);

    }

  }); // end #logo_nav.click function

  // When the pause button is clicked
  $('#logo_nav_pause').click(function(e){

    $('#songcanvas').hide();
    $('#bg').show();

    $('#logo_nav_pause').hide();
    $('#logo_nav').show();

    audio.pause();

  });

  // When the song has finished and the user clicks to restart the song
  $('#logo_nav_restart').click(function(e){

      audio.remove();

      $('#logo_nav_restart').hide();
      $('#logo_nav_pause').show();
  
      $('#bg').hide();
      $('#songcanvas').show();
  
      e.preventDefault();
      var path = "http://upload.wikimedia.org/wikipedia/en/4/45/ACDC_-_Back_In_Black-sample.ogg";
      loadSong(path);

  });

});